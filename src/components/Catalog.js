import React from 'react';

const Catalog = ({categories,products,handleAddToCart}) => {
	
	
	return (

		<div className="container">
			<h1>Products</h1>
			<div className="row">
				
					{
						products.map( product => {
							return (
								<div className="col-12 col-md-3">
									<div className="card">
										<div className="card-header">
											<img src={"http://localhost:3001" + product.image} height="200" width = "200" alt="images"/>
										</div>
										<div className="card-body">
											<p className="card-item">{product.name}</p>
											<p className="card-item">{product.price}</p>
											<p className="card-item">{product.description}</p>
											<button className="btn btn-dark" type="submit" onClick={()=>{handleAddToCart(product)}}>Add to Cart</button>
										</div>

									</div>
								</div>
							)
						})
					}
				
			</div>
		</div>
	)
}

export default Catalog;