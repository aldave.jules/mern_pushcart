import React from 'react';
import AddProduct from './forms/AddProductForm';
import EditProduct from './forms/EditProductForm';
import DeleteProduct from './forms/DeleteProductForm';
import AddCategory from './forms/AddCategoryForm';
import EditCategory from './forms/EditCategoryForm';
import DeleteCategory from './forms/DeleteCategoryForm';

const AdminPanel = ({categories, handleChangeCategoryStatus,categoryStatus,products}) => {
	
	return (
		<div className="container">
			<div className="row">
				<div className="col-lg-8">
					<div className="row">
						<div className="col-lg-12">
							<AddProduct categories={categories}/>
							<EditProduct categories={categories} products={products}/>
							<DeleteProduct products={products}/>
						</div>
					</div>
				</div>
				<div className="col-lg-4">
					<div className="row sticky-top">
						<div className="col-lg-12">
							<AddCategory handleChangeCategoryStatus={handleChangeCategoryStatus}/>
							<EditCategory categories={categories} handleChangeCategoryStatus={handleChangeCategoryStatus} categoryStatus={categoryStatus}/>
							<DeleteCategory categories={categories} handleChangeCategoryStatus={handleChangeCategoryStatus}/>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default AdminPanel;