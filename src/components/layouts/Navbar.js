import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';

const Navbar = () => {
	return (
		<Fragment>
			<nav className="navbar navbar-dark bg-dark navbar-expand-lg">
				<Link to="/" className="navbar-brand">Pushcart</Link>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarNav">
					<ul className="navbar-nav ml-auto">
						<li className="nav-item">
							<Link className="nav-link" to="/">Catalog</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" to="/transaction">Transaction</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" to="/cart">Cart</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" to="/admin-panel">Admin Panel</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" to="/register">Register</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" to="/login">Login</Link>
						</li>
					</ul>
				</div>
			</nav>
		</Fragment>
	)
}

export default Navbar;