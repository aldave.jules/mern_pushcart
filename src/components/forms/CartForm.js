import React from 'react';

const Cart = ({cart,handleClearCart,handleRemoveItem}) => {
	const handleCheckout=()=>{
		let orders = cart.map(item=>{
			return {
				id: item._id,
				qty: 1
			}
		})
		fetch("http://localhost:3001/transactions",{
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem('token')
			},
			body: JSON.stringify({orders})
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}

	let total = 0;

	return(
		<div className="container">
			<div className="row">
				<div className="col-10 offset-1">
					{
						cart.length ? 
							<div>
								<h1 className="mt-5">Cart</h1>
								<table className="table table-striped mt-3 mb-5 text-center">
									<thead>
										<tr>
											<th scope="col">Name</th>
											<th scope="col">Price</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>
									{
										
										cart.map(item => {
											total+=item.price
											return(
											<tr>
												<td>{item.name}</td>
												<td>&#8369; {item.price}</td>
												<td><button className="btn btn-outline-danger" onClick={()=>{handleRemoveItem(item)}}>Remove from Cart</button></td>
											</tr>
											)
										})
									}
										
										<tr>
											<td className="text-right">Total:</td>
											<td>&#8369; {total}</td>
											<td><button className="btn btn-success" onClick={handleCheckout}>Checkout</button></td>
										</tr>
									</tbody>
								</table>
								<button className="btn btn-danger" onClick={handleClearCart}>Clear Cart</button>
							</div>
							: 
							<h3>Cart is empty</h3>
					}

				</div>
			</div>
		</div>
	)
}

export default Cart;