import React, {useState} from 'react';

const DeleteProduct = ({products}) => {
	const [selectedProduct, setSelectedProduct] = useState({
		name: null,
		categoryId: null,
		description: null,
		image: null,
		price: null,
		id: null
	})

	const handleChangeSelected = e => {

		let productSelected = products.find(product => {
			return product._id === e.target.value
		})
		setSelectedProduct({
			id: e.target.value,
			name: productSelected.name,
			price: productSelected.price,
			description: productSelected.description,
			categoryId: productSelected.categoryId,
			image: productSelected.image
		})
	}

	const handleDeleteProduct = e => {
		e.preventDefault()

		fetch("http://localhost:3001/products/" + selectedProduct.id,
		{
			method: "DELETE",
			headers: {
				"Authorization": localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}

	return(
	<div className="mb-5">
		<h2 className="text-center mt-3">Delete Product</h2>
		<form onSubmit={handleDeleteProduct}>
			<div className="card">
				<div className="card-body">
					<div className="row">
						<div className="col-8">
							<select name="name" id="name" className="form-control" onChange={handleChangeSelected}>
								<option disabled selected>Select Product</option>
								{	products.map(product => {
										return(
											<option value={product._id}>{product.name}</option>
										)
									})
								}
							</select>
						</div>
						<div className="col-4">
							<button className="btn btn-danger" type="submit">Delete</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	)
}

export default DeleteProduct;