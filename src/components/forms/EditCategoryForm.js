import React, {useState} from 'react';

const EditCategory = ({categories,handleChangeCategoryStatus,categoryStatus}) => {
	const [selectedCategory, setSelectedCategory] = useState({
		name:null,id:null
	})

	const handleChangeSelected = (e) => {
		let categorySelected = categories.find( category => {
			return category._id === e.target.value 
		})
		setSelectedCategory({
			id: e.target.value,
			name: categorySelected.name
		})
	}

	const handleChangeName = (e) => {
		setSelectedCategory({
			...selectedCategory,
			[e.target.name]: e.target.value
		})
	}

	const handleEditCategory = e => {
		e.preventDefault()

		fetch("http://localhost:3001/categories/" + selectedCategory.id,
		{
			method: "PUT",
			body: JSON.stringify({name: selectedCategory.name}),
			headers: {
				"Content-Type" : "application/json",
				"Authorization": localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => handleChangeCategoryStatus({
			lastUpdated: selectedCategory.id,
			status: "pass",
			isLoading:true
		}))
	}

	return(
	<div>
		<h2 className="text-center mt-3">Edit Category</h2>

		<div className="alert d-none" id="message" role="alert">
		</div>

		{
			categoryStatus.isLoading ?

				<div class="spinner-border" role="status">
					<span class="sr-only">Loading...</span>
				</div>
			: 
				<React.Fragment>
						<div className="card">
							<div className="card-body">
								<form onSubmit={handleEditCategory}>
									<div className="form-group">
										<label htmlFor="name">Current Category Name:</label>
										<select onChange={handleChangeSelected} name="name" id="name" className="form-control">
											<option disabled selected>Select Category</option>
											{	categories.map(category => {
													return(
														<option value={category._id}>{category.name}</option>
													)
												})
											}
										</select>
									</div>
									<div className="form-group">
										<label htmlFor="name">New Category Name:</label>
										<input type="text" id="name" name="name" className="form-control" value={selectedCategory.name} onChange={handleChangeName}/>
									</div>
									<button type="submit" className="btn btn-dark">Edit Category</button>
								</form>
							</div>
						</div>
				</React.Fragment>
		}
		
	</div>
	)
}

export default EditCategory;