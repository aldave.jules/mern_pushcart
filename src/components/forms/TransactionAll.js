import React, {useState,useEffect} from 'react';


const Transaction = () => {
	const [transactions,setTransactions] = useState([])
	const [status,setStatus] = useState({})

	const handleChangeSelectStatus = e => {
		setStatus({status: e.target.value})
	}

	const handleEditStatus = (transactionId) => {
		console.log(transactionId)
		fetch('http://localhost:3001/transactions/' + transactionId,{
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem('token')
			},
			body: JSON.stringify(status)
		})
		.then(data=>data.json())
		.then(result => console.log(result)) 
	}

	useEffect(()=> {
		fetch('http://localhost:3001/transactions',{
			headers: {
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then(data=>data.json())
		.then(transactions => setTransactions(transactions))
	},[])

	return(
		<div className="container ">
			<div className="row">
				<div className="col-8 offset-2 mt-3">
					<h3>Transactions</h3>
						<div class="accordion" id="accordionExample">
							{
								transactions.map(transaction => {
									return(
							  				<div class="card">
											    <div class="card-header" id="headingOne">
											      	<h2 class="mb-0">
											        	<button class="btn btn-link" type="button" data-toggle="collapse" data-target={"#collapseOne"+transaction._id} aria-expanded="true" aria-controls="collapseOne">
											          		<span className="badge badge-primary">Pending</span>
											        	</button>
											      	</h2>
											    </div>
											<div id={"collapseOne"+transaction._id} class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
										      	<div class="card-body">
										      		<table className="table table-striped mt-3 mb-5 text-center">
														<tbody>
															<tr>
																<td>User</td>
																<td>{transaction.userId}</td>
															</tr>
															<tr>
																<td>Transaction Code</td>
																<td>{transaction.transactionCode}</td>
															</tr>
															<tr>
																<td>Payment Mode</td>
																<td>{transaction.paymentMode}</td>
															</tr>
															<tr>
																<td>Date</td>
																<td>{transaction.createdAt}</td>
															</tr>
															<tr>
																<td>Status</td>
																<td>{transaction.status}
																	<select name="Pending" className="form-control" onChange={handleChangeSelectStatus}>
																		<option value="Pending">Pending</option>
																		<option value="Done">Done</option>
																	</select>
																	
																	<button className="btn btn-outline-warning" onClick={()=>{handleEditStatus(transaction._id)}}>
																		Edit Status
																	</button>
																</td>
															</tr>
														</tbody>
													</table>
										        	<table className="table table-striped mt-3 mb-5 text-center">
														<thead>
															<tr>
																<th scope="col">Name</th>
																<th scope="col">Price</th>
															</tr>
														</thead>
														<tbody>

														{

															transaction.products.map(transItem => {
																return(
																	<tr>
																		<td>{transItem.name}</td>
																		<td>{transItem.price}</td>
																	</tr>
																)
															})
														}
															<tr>
																<td className="text-right">Total:</td>
																<td>&#8369;{transaction.total}</td>
															</tr>
														</tbody>
													</table>
										      	</div>
										    </div>
									    </div>
									)
								})
							}
							</div>	
						</div>
					</div>
				</div>
	)
}

export default Transaction;