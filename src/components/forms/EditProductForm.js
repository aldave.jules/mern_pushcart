import React, {useState} from 'react';

const EditProduct = ({categories,products}) => {
	
	const [selectedProduct, setSelectedProduct] = useState({
		name: null,
		categoryId: null,
		description: null,
		image: null,
		price: null
	})

	const handleChangeFile = e => {
		setSelectedProduct({
			...selectedProduct,
			image: e.target.files[0]
		})
	}

	const handleChangeSelected = e => {

		let productSelected = products.find(product => {
			return product._id === e.target.value
		})
		setSelectedProduct(productSelected)
	}
	

	const handleChange = e => {
		setSelectedProduct({
			...selectedProduct,
			[e.target.name]: e.target.value
		})
	}

	const handleEditProduct = e => {
		e.preventDefault()

		const formData = new FormData();

		formData.append('name', selectedProduct.name)
		formData.append('image', selectedProduct.image)
		formData.append('description', selectedProduct.description)
		formData.append('categoryId', selectedProduct.categoryId)
		formData.append('price', selectedProduct.price)

		fetch("http://localhost:3001/products/" + selectedProduct._id, {
			method: "PUT",
			body: formData,
			headers: {
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}

	return(
	<div>
		<h2 className="text-center mt-3">Edit Product</h2>

		<div className="alert d-none" id="message" role="alert">
		</div>

		<div className="card">
			<div className="card-body">
				<form onSubmit={handleEditProduct} enctype="multipart/form-data">
					<div className="form-group">
						<label htmlFor="image">Select Product to Edit:</label>
						<select name="name" id="name" className="form-control" onChange={handleChangeSelected}>
							<option disabled selected>Select Product</option>
							{	products.map(product => {
									return(
										<option value={product._id}>{product.name}</option>
									)
								})
							}
						</select>
					</div>
					<div className="form-group">
						<label htmlFor="image">Product Image:</label>
						<img src={"http://localhost:3001" + selectedProduct.image} alt="images"/>
						<input type="file" id="image" name="image" className="form-inline" onChange={handleChangeFile}/>
					</div>
					<div className="form-group">
						<label htmlFor="name">Product Name:</label>
						<input type="text" id="name" name="name" className="form-control" onChange={handleChange} value={selectedProduct.name}/>
					</div>
					<div className="form-group">
						<label htmlFor="categoryId">Product Category:</label>
						<select name="categoryId" id="categoryId" className="form-control">
							<option disabled selected>Select Category</option>
							{	categories.map(category => {
									return(
										category._id === selectedProduct.categoryId ?
										<option value={category._id} selected>{category.name}</option> :
										<option value={category._id}>{category.name}</option>
									)
								})
							}
						</select>
					</div>
					<div className="form-group">
						<label htmlFor="price">Product Price:</label>
						<input type="number" id="price" name="price" className="form-control"  onChange={handleChange} value={selectedProduct.price}/>
					</div>
					<div className="form-group">
						<label htmlFor="description">Product Description:</label>
						<textarea id="description" name="description" className="form-control"  onChange={handleChange} value={selectedProduct.description}></textarea>
					</div>
					<button type="submit" className="btn btn-dark">Edit Product</button>
				</form>
			</div>
		</div>
	</div>
	)
}

export default EditProduct;