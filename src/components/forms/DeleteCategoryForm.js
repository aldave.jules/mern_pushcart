import React, {useState} from 'react';

const DeleteCategory = ({categories,handleChangeCategoryStatus}) => {
	const [selectedCategory, setSelectedCategory] = useState({
		name:null,id:null
	})

	const handleChangeSelected = e => {
		let categorySelected = categories.find(category => {
			return category._id === e.target.value
		})
		setSelectedCategory({
			id: e.target.value,
			name: categorySelected.name
		})
	}

	const handleDeleteCategory = e => {
		e.preventDefault()

		fetch("http://localhost:3001/categories/" + selectedCategory.id,
		{
			method: "DELETE",
			headers: {
				"Authorization": localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => handleChangeCategoryStatus({
			lastUpdated: selectedCategory.id,
			status: "pass"
		}))
	}


	return(
	<div>
		<h2 className="text-center mt-3">Delete Category</h2>
		<div>
			<div className="card">
				<div className="card-body">
					<form onSubmit={handleDeleteCategory}>
						<div className="row">
							<div className="col-8">
								<select name="name" id="name" className="form-control" onChange={handleChangeSelected}>
									{	categories.map(category => {
											return(
												<option value={category._id}>{category.name}</option>
											)
										})
									}
								</select>
							</div>
							<div className="col-4">
								<button className="btn btn-danger">Delete</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	)
}

export default DeleteCategory;