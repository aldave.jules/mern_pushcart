import React, {useState} from 'react';

const AddCategory = ({handleChangeCategoryStatus}) => {

	const [formData, setFormData] = useState("")

	const handleAddCategory = (event) => {
		event.preventDefault()
		
		let url = "http://localhost:3001/categories"
		let data = {name: formData}
		let token = localStorage.getItem('token')

		fetch(url, {
			method: "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : token
			}
		})
		.then(data => data.json())
		.then(result => handleChangeCategoryStatus({
			lastUpdated: result.name,
			status: "pass"
		}))
	}

	const handleCategoryNameChange = (event) => {
		setFormData(event.target.value)
	}

	return(
	<div>
		<h2 className="text-center mt-5">Add Category</h2>

		<div className="alert d-none" id="message" role="alert">
		</div>

		<div className="card">
			<div className="card-body">
				<form onSubmit={handleAddCategory}>
					<div className="form-group">
						<label htmlFor="name">Category Name:</label>
						<input type="text" id="name" name="name" className="form-control" onChange={handleCategoryNameChange}/>
					</div>
					<button type="submit" className="btn btn-dark">Add Category</button>
				</form>
			</div>
		</div>

	</div>
	)
}

export default AddCategory;