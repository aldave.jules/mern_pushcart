import React, {useState} from 'react';

const Login = () => {
	const [formData, setFormData] = useState({
		email: null,
		password:null
	})

	const [result, setResult] = useState({
		message: null,
		successful: null
	})

	const handleChange = e => {
		setFormData({
			...formData,
			[e.target.name] : e.target.value
		})
	}

	const handleLogin = e => {
		e.preventDefault();
		
		let data = {
			email: formData.email,
			password: formData.password
		}

		fetch("http://localhost:3001/users/login",{
			method: "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then(data => data.json())
		.then(user => {
			console.log(user)

			if(user.token){
				setResult({
					successful: true, 
					message: user.message
				})
				localStorage.setItem('firstname', JSON.stringify(user.user));
				localStorage.setItem('token', "Bearer "+user.token);
			} else {
				setResult({
					successful: false, 
					message: user.message
				})
			}
		})
	}

	const resultMessage = () => {
		let classList;
		if (result.successful === true){
			classList = 'alert alert-success'
		}else {
			classList = 'alert alert-warning'
		}

		return (
			<div className={classList}>
				{result.message}
			</div>
		)
	}
	return (
		<div className="container">
			<div className="row">
				<div className="col-lg-6 mx-auto my-auto">
					<h2 className="text-center mt-5 mb-5">Login Page</h2>
					{result.message === null ? "" : resultMessage()}
					<div className="container">
						<div className="card">
							<div className="card-body">
								<form onSubmit={handleLogin}>
									<div className="form-group">
										<label htmlFor="email">Email Address:</label>
										<input type="email" id="email" name="email" className="form-control" onChange={handleChange}/>
									</div>
									<div className="form-group">
										<label htmlFor="password">Password:</label>
										<input type="password" id="password" name="password" className="form-control" onChange={handleChange}/>
									</div>
									<button type="submit" className="btn btn-dark">Login</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Login;