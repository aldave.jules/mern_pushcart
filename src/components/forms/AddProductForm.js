import React, {useState} from 'react';

const AddProduct = ({categories}) => {

	const [product, setProduct] =  useState({
		name: null,
		image: null,
		categoryId: null,
		price: null,
		description: null
	})

	const onChangeText = e => {
		setProduct({
			...product,
			[e.target.name] : e.target.value
		})
	}

	const handleChangeFile = e => {
		setProduct({
			...product,
			image: e.target.files[0]
		})
	}
	
	const handleAddProduct = e => {
		e.preventDefault()

		const formData = new FormData();

		formData.append('name', product.name)
		formData.append('image', product.image)
		formData.append('description', product.description)
		formData.append('categoryId', product.categoryId)
		formData.append('price', product.price)

		fetch("http://localhost:3001/products", {
			method: "POST",
			body: formData,
			headers: {
				"Authorization" : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => console.log(result))
	}

	return(
	<div>
		<h2 className="text-center mt-5">Add Product</h2>

		<div className="alert d-none" id="message" role="alert">
		</div>


		<div className="card">
			<div className="card-body">
				<form enctype="multipart/form-data" onSubmit={handleAddProduct}>
					<div className="form-group">
						<label htmlFor="name">Product Name:</label>
						<input type="text" id="name" name="name" className="form-control" onChange={onChangeText}/>
					</div>
					<div className="form-group">
						<label htmlFor="image">Product Image:</label>
						<input type="file" id="image" name="image" className="form-inline" onChange={handleChangeFile}/>
					</div>
					<div className="form-group">
						<label htmlFor="categoryId">Product Category:</label>
						<select name="categoryId" id="categoryId" className="form-control" onChange={onChangeText}>
							<option disabled selected>Select Category</option>
							{	categories.map(category => {
									return(
										<option value={category._id}>{category.name}</option>
									)
								})
							}
						</select>
					</div>
					<div className="form-group">
						<label htmlFor="price">Product Price:</label>
						<input type="number" id="price" name="price" className="form-control" onChange={onChangeText}/>
					</div>
					<div className="form-group">
						<label htmlFor="description">Product Description:</label>
						<textarea id="description" name="description" className="form-control" onChange={onChangeText}></textarea>
					</div>
					<button type="submit" className="btn btn-dark">Add Product</button>
				</form>
			</div>
		</div>
	</div>
	)
}

export default AddProduct;