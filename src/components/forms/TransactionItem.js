import React from 'react'

const TransactionItem = () => {
	return(
		<div>
			<table className="table table-striped mt-3 mb-5 text-center">
				<thead>
					<tr>
						<th scope="col">Name</th>
						<th scope="col">Price</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td className="text-right">Total:</td>
						<td>&#8369;</td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default TransactionItem