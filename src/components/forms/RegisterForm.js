import React, {useState} from 'react';

const Register = () => {
	
	const [formData, setFormData] = useState({
		firstname: "",
		lastname: "",
		email: "",
		password: "",
		confirmPassword: ""
	})

	const {firstname,lastname,email,password,confirmPassword} = formData;

	const onChangeHandler = event => {
		setFormData({
			...formData,
			[event.target.name]: event.target.value
		})
	}

	const handleRegister = event => {
		event.preventDefault();

		if(password !== confirmPassword){
			alert("Password do not match")
		} else {
			fetch("http://localhost:3001/users/register",{
				method: "POST",
				headers: {
					"Content-Type" : "application/json"
				},
				body: JSON.stringify(formData)
			})
			.then(data=>data.json())
			.then(user=>{
				console.log(user.message)
				setFormData({
					firstname: "",
					lastname: "",
					email: "",
					password: "",
					confirmPassword: ""
				})

				if(user.message){
					let element = document.getElementById("message")
					element.innerHTML = user.message
					element.classList.toggle("d-none")
					element.classList.add("alert-danger")
					element.classList.remove("alert-success")
					setTimeout(function(){element.classList.toggle("d-none")},5000)
				} else {
					let element = document.getElementById("message")
					element.innerHTML = user.messageSuccess
					element.classList.toggle("d-none")
					element.classList.add("alert-success")
					element.classList.remove("alert-danger")
					setTimeout(function(){element.classList.toggle("d-none")},15000)
				}
			})
		}
	}
	return (
		<div>
			<div className="col-lg-6 mx-auto">
				<h2 className="text-center mt-5 mb-5">Registration Page</h2>

				<div className="alert d-none" id="message" role="alert">
				</div>

				<div className="container">
					<div className="card">
						<div className="card-body">
							<form onSubmit={event => handleRegister(event)}>
								<div className="form-group">
									<label htmlFor="firstname">First Name:</label>
									<input type="text" id="firstname" name="firstname" value={firstname} onChange={onChangeHandler} className="form-control"/>
								</div>
								<div className="form-group">
									<label htmlFor="lastname">Last Name:</label>
									<input type="text" id="lastname" name="lastname" value={lastname} onChange={onChangeHandler} className="form-control"/>
								</div>
								<div className="form-group">
									<label htmlFor="email">Email Address:</label>
									<input type="text" id="email" name="email" value={email} onChange={onChangeHandler} className="form-control"/>
								</div>
								<div className="form-group">
									<label htmlFor="password">Password:</label>
									<input type="text" id="password" name="password" value={password} onChange={onChangeHandler} className="form-control"/>
								</div>
								<div className="form-group">
									<label htmlFor="confirmPassword">Confirm Password:</label>
									<input type="text" id="confirmPassword" name="confirmPassword" value={confirmPassword} onChange={onChangeHandler} className="form-control"/>
								</div>
								<button type="submit" className="btn btn-dark">Register</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Register;