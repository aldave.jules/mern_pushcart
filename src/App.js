import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Navbar from './components/layouts/Navbar';
import Catalog from './components/Catalog';
import AdminPanel from './components/AdminPanel';
import Register from './components/forms/RegisterForm';
import Login from './components/forms/LoginForm';
import Cart from './components/forms/CartForm';
import Transaction from './components/forms/TransactionAll';

const App = () => {

  const [products, setProducts] = useState([])
  const [categories, setCategories] = useState([])
  const [cart,setCart] = useState([])
  const [categoryStatus, setCategoryStatus] = useState({
    lastUpdated: null,
    status:null, 
    isLoading: true
  })

  useEffect(()=>{
    let token = localStorage.getItem('token')
    fetch("http://localhost:3001/categories",{
      method: "GET",
      headers: {
        "Authorization" : token
      }
    })
    .then(data=>data.json())
    .then(category => {
      setCategories(category)
      setCategoryStatus({isLoading:false})
    })
  },[categoryStatus.isLoading])


  useEffect( () => {
    fetch("http://localhost:3001/products",{
      method: "GET"
    })
    .then(data=>data.json())
    .then(product => setProducts(product))
    
    let token = localStorage.getItem('token')

    fetch("http://localhost:3001/categories",{
      method: "GET",
      headers: {
        "Authorization" : token
      }
    })
    .then(data=>data.json())
    .then(category => setCategories(category))

  },[])
  
  const handleChangeCategoryStatus = status => {
    setCategoryStatus(status)
  }
  
  const handleAddToCart = (product) => {
    let matched = cart.find(item => {
      return item._id === product._id
    })
    console.log(matched)
    if(!matched){
      setCart([
        ...cart,
        product])   
    }
  }
  
  const handleClearCart = () => {
    setCart([])
  }

  const handleRemoveItem = (itemRemoved) => {
    let updatedCart = cart.filter(item => 
      item !== itemRemoved
    )
    setCart(updatedCart);
  }
  return (
    <Router>
      
      <Navbar/>
      <Switch>
        <Route exact path="/"><Catalog categories={categories} products={products} handleAddToCart={handleAddToCart}/></Route>
        <Route exact path="/transaction"><Transaction/></Route>
        <Route exact path="/admin-panel"><AdminPanel 
          categories={categories}
          handleChangeCategoryStatus={handleChangeCategoryStatus}
          categoryStatus={categoryStatus}
          products={products}
        /></Route>
        <Route exact path="/register"><Register/></Route>
        <Route exact path="/login"><Login/></Route>
        <Route exact path="/cart"><Cart cart={cart} handleClearCart={handleClearCart} handleRemoveItem={handleRemoveItem}/></Route>
      </Switch>
    </Router>
  );
}

export default App;
